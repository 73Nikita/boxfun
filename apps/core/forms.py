# coding: utf8
from django import forms
from models import BackCall, Question, Review, Order, Certificate


class BackCallForm(forms.ModelForm):
    class Meta:
        model = BackCall
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Введите Ваше имя', 'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'placeholder': u'Контактный телефон', 'class': 'form-control phone'}),
        }


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Введите Ваше имя', 'class': 'form-control'}),
            'contacts': forms.TextInput(attrs={'placeholder': u'Контактный телефон', 'class': 'form-control phone'}),
            'text': forms.Textarea(attrs={'placeholder': u'Ваш вопрос', 'class': 'textarea', 'rows': '10'}),
        }


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Введите Ваше имя', 'class': 'form-control'}),
            'contacts': forms.TextInput(attrs={'placeholder': u'Контактный телефон', 'class': 'form-control phone'}),
            'text': forms.Textarea(attrs={'placeholder': u'Ваш отзыв', 'class': 'textarea', 'rows': '10'}),
        }


class CertificateForm(forms.ModelForm):
    class Meta:
        model = Certificate
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Введите Ваше имя', 'class': 'form-control'}),
            'contacts': forms.TextInput(attrs={'placeholder': u'Контактный телефон', 'class': 'form-control phone'}),
        }


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('name', 'contacts', 'size', 'theme', 'sex', 'costume', 'payment_status', 'order_type')
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Введите Ваше имя', 'class': 'form-control'}),
            'contacts': forms.TextInput(attrs={'placeholder': u'Контактный телефон', 'class': 'form-control phone'}),
            'size': forms.Select(attrs={'class': 'selectpicker form-select', 'title': 'M'}),
            'theme': forms.CheckboxSelectMultiple(attrs={'class': 'theme-checkbox checkbox'}),
            'sex': forms.CheckboxInput(attrs={'class': 'checkbox'}),
            'costume': forms.CheckboxInput(attrs={'class': 'checkbox'}),
        }
        labels = {
            'size': u'Выберите размер',
        }
