# coding: utf8
from threading import Thread
from django.core.mail import send_mail, EmailMessage
from django.db import models
from solo.models import SingletonModel

EMAIL = 'boxfun.ru@gmail.com'


def send_mail_thread(title, text):
    """ Функция отправки уведомлений на почту администратора.
    Вызывается в отдельном потоке """
    print '123'
    send_mail(title, text, EMAIL, [Settings.get_solo().email_admin], fail_silently=False)
    print 'Email send'


class BackCall(models.Model):
    """ Модель заказанных обратных звонков """
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    phone = models.CharField(verbose_name=u'Номер', max_length=255)
    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(BackCall, self).save()
        title = u'Обратный звонок'
        text = u'Пользователь: %s\nКонтактные данные: %s' % (self.name, self.phone)
        t = Thread(target=send_mail_thread, kwargs={'title': title, 'text': text})
        t.start()

    class Meta:
        verbose_name = u'Заказанный звонок'
        verbose_name_plural = u'Заказанные звонки'


class Question(models.Model):
    """ Модель "задать вопрос" """
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    contacts = models.CharField(verbose_name=u'Контактные данные', max_length=1024)
    text = models.TextField(verbose_name=u'Вопрос')
    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Question, self).save()
        title = u'Вопрос'
        text = u'Пользователь: %s\nКонтактные данные: %s\nВопрос: %s' % (self.name, self.contacts, self.text)
        t = Thread(target=send_mail_thread, kwargs={'title': title, 'text': text})
        t.start()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'


class Review(models.Model):
    """ Модель отзыва """
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    contacts = models.CharField(verbose_name=u'Контактные данные', max_length=1024)
    text = models.TextField(verbose_name=u'Отзыв')
    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Review, self).save()
        title = u'Отзыв'
        text = u'Пользователь: %s\nКонтактные данные: %s\nОтзыв: %s' % (self.name, self.contacts, self.text)
        t = Thread(target=send_mail_thread, kwargs={'title': title, 'text': text})
        t.start()

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'


class Certificate(models.Model):
    """ Модель "заказать сертификат" """
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    contacts = models.CharField(verbose_name=u'Контактные данные', max_length=1024)
    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Certificate, self).save()
        title = u'Сертификат'
        text = u'Пользователь: %s\nКонтактные данные: %s' % (self.name, self.contacts)
        t = Thread(target=send_mail_thread, kwargs={'title': title, 'text': text})
        t.start()

    class Meta:
        verbose_name = u'Заказанный сертификат'
        verbose_name_plural = u'Заказанные сертификаты'


PAYMENT_STATUS = (
    (0, u'Форма заполнена'),
    (1, u'Нажата кнопка оплатить'),
    (2, u'Отказ от оплаты'),
    (3, u'Пользователь оплатил подписку'),
)

SIZE = (
    (0, 'S'),
    (1, 'M'),
    (2, 'L'),
    (3, 'XL'),
    (4, 'XXL'),
)

SEX = (
    (True, u'Женщина'),
    (False, u'Мужчина'),
)

COSTUME = (
    (True, u'Толстовка'),
    (False, u'Футболка'),
)

ORDER_TYPE = (
    (0, u'3 месяца'),
    (1, u'6 месяцев'),
    (2, u'12 месяцев'),
)


class Order(models.Model):
    """ Модель заказа """
    name = models.CharField(verbose_name=u'Имя', max_length=255)
    contacts = models.CharField(verbose_name=u'Контактные данные', max_length=1024)
    payment_status = models.PositiveSmallIntegerField(verbose_name=u'Статус оплаты', default=0, choices=PAYMENT_STATUS)
    size = models.PositiveSmallIntegerField(verbose_name=u'Размер', default=0, choices=SIZE)
    theme = models.ManyToManyField(to='Theme', verbose_name=u'Тематика дизайна', blank=True)
    sex = models.BooleanField(verbose_name=u'Пол', default=True, choices=SEX)
    costume = models.BooleanField(verbose_name=u'Тип одежды', default=False, choices=COSTUME)
    order_type = models.SmallIntegerField(verbose_name=u'Тип подписки', default=0, choices=ORDER_TYPE)
    old_price = models.FloatField(verbose_name=u'Сумма к оплате', default=0, blank=True)
    date = models.DateTimeField(verbose_name=u'Дата', auto_now_add=True)
    sent = models.BooleanField(verbose_name=u'Отправлено сообщение', blank=True, default=False)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Order, self).save()
        if (self.payment_status >= 1 and self.sent is False) or self.payment_status >= 2:
            themes = ''
            for t in self.theme.all():
                themes += unicode(t) + u', '

            text = u'Пользователь: %s\n' % self.name
            text += u'Контактные данные: %s\n' % self.contacts
            text += u'Статус оплаты: %s\n' % self.get_payment_status_display()
            text += u'Размер: %s\n' % self.get_size_display()
            text += u'Тематика: ' + themes[:-2] + '\n'
            text += u'Пол: %s\n' % self.get_sex_display()
            text += u'Тип одежды: %s\n' % self.get_costume_display()
            text += u'Срок подписки: %s\n' % self.get_order_type_display()

            title = u'Оформление подписки'
            t = Thread(target=send_mail_thread, kwargs={'title': title, 'text': text})
            t.start()

            if self.sent is False:
                self.sent = True
                self.save()

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'


class Settings(SingletonModel):
    """ Настройки сайта """
    email_admin = models.EmailField(verbose_name=u'E-mail', blank=True)
    t_shirt_order_one = models.FloatField(verbose_name=ORDER_TYPE[0][1], default=4980, help_text=u'руб')
    t_shirt_order_two = models.FloatField(verbose_name=ORDER_TYPE[1][1], default=17770, help_text=u'руб')
    t_shirt_order_three = models.FloatField(verbose_name=ORDER_TYPE[2][1], default=35550, help_text=u'руб')
    sweatshirt_order_one = models.FloatField(verbose_name=ORDER_TYPE[0][1], default=4980, help_text=u'руб')
    sweatshirt_order_two = models.FloatField(verbose_name=ORDER_TYPE[1][1], default=17770, help_text=u'руб')
    sweatshirt_order_three = models.FloatField(verbose_name=ORDER_TYPE[2][1], default=35550, help_text=u'руб')

    class Meta:
        verbose_name = u'Настройка сайта'
        verbose_name_plural = u'Настройки сайта'


class Result(models.Model):
    """ Модель для сохранения запросов сервиса оплаты """
    data = models.TextField(verbose_name=u'Результат')
    date = models.DateTimeField(verbose_name=u'Дата и время', auto_now_add=True)

    class Meta:
        verbose_name = u'Результат ответа'
        verbose_name_plural = u'Результаты ответов'


class Theme(models.Model):
    """ Модель тематик """
    title = models.CharField(verbose_name=u'Название', max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Тематика дизайна'
        verbose_name_plural = u'Тематики дизайна'
