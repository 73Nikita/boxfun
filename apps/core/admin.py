# coding: utf8
from django.contrib import admin
from solo.admin import SingletonModelAdmin
from models import BackCall, Question, Review, Order, Certificate, Theme, Settings, Result


class OrderAdmin(admin.ModelAdmin):
    list_display = ('name', 'payment_status', 'size', 'sex', 'costume', 'old_price', 'date', 'sent')
    list_filter = ('payment_status', 'sex', 'costume', 'date',)
    readonly_fields = ('date',)
    filter_horizontal = ('theme',)


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('name', 'date',)
    readonly_fields = ('date',)
    list_filter = ('date',)


class SettingsAdmin(SingletonModelAdmin):
    fieldsets = (
        (u'Стоимость подписок на футболку', {
            'fields': ('t_shirt_order_one', 't_shirt_order_two', 't_shirt_order_three',)
        }),
        (u'Стоимость подписок на толстовку', {
            'fields': ('sweatshirt_order_one', 'sweatshirt_order_two', 'sweatshirt_order_three',)
        }),
        (None, {
            'fields': ('email_admin',)
        })
    )


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('name', 'date',)
    readonly_fields = ('date',)
    list_filter = ('date',)


class CertificateAdmin(admin.ModelAdmin):
    list_display = ('name', 'date',)
    readonly_fields = ('date',)
    list_filter = ('date',)


class BackCallAdmin(admin.ModelAdmin):
    list_display = ('name', 'date',)
    readonly_fields = ('date',)
    list_filter = ('date',)


class ResultAdmin(admin.ModelAdmin):
    list_display = ('data', 'date',)
    readonly_fields = ('date',)

admin.site.register(BackCall, BackCallAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Certificate, CertificateAdmin)
admin.site.register(Theme)
admin.site.register(Settings, SettingsAdmin)
admin.site.register(Result, ResultAdmin)
