from django.conf.urls import url
from views import IndexView, ResultView, VerificationView


urlpatterns = [
    url(r'w1_138194063012\.txt$', VerificationView.as_view()),
    url(r'^result/', ResultView.as_view(), name='result'),
    url(r'^$', IndexView.as_view(), name='index')
]
