# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_result'),
    ]

    operations = [
        migrations.AddField(
            model_name='settings',
            name='email_admin',
            field=models.EmailField(max_length=254, verbose_name='E-mail', blank=True),
        ),
    ]
