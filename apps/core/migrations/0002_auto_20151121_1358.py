# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='settings',
            name='order_one',
        ),
        migrations.RemoveField(
            model_name='settings',
            name='order_three',
        ),
        migrations.RemoveField(
            model_name='settings',
            name='order_two',
        ),
        migrations.AddField(
            model_name='settings',
            name='sweatshirt_order_one',
            field=models.FloatField(default=4980, help_text='\u0440\u0443\u0431', verbose_name='3 \u043c\u0435\u0441\u044f\u0446\u0430'),
        ),
        migrations.AddField(
            model_name='settings',
            name='sweatshirt_order_three',
            field=models.FloatField(default=35550, help_text='\u0440\u0443\u0431', verbose_name='12 \u043c\u0435\u0441\u044f\u0446\u0435\u0432'),
        ),
        migrations.AddField(
            model_name='settings',
            name='sweatshirt_order_two',
            field=models.FloatField(default=17770, help_text='\u0440\u0443\u0431', verbose_name='6 \u043c\u0435\u0441\u044f\u0446\u0435\u0432'),
        ),
        migrations.AddField(
            model_name='settings',
            name='t_shirt_order_one',
            field=models.FloatField(default=4980, help_text='\u0440\u0443\u0431', verbose_name='3 \u043c\u0435\u0441\u044f\u0446\u0430'),
        ),
        migrations.AddField(
            model_name='settings',
            name='t_shirt_order_three',
            field=models.FloatField(default=35550, help_text='\u0440\u0443\u0431', verbose_name='12 \u043c\u0435\u0441\u044f\u0446\u0435\u0432'),
        ),
        migrations.AddField(
            model_name='settings',
            name='t_shirt_order_two',
            field=models.FloatField(default=17770, help_text='\u0440\u0443\u0431', verbose_name='6 \u043c\u0435\u0441\u044f\u0446\u0435\u0432'),
        ),
        migrations.AlterField(
            model_name='order',
            name='size',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440', choices=[(0, b'S'), (1, b'M'), (2, b'L'), (3, b'XL'), (4, b'XXL')]),
        ),
    ]
