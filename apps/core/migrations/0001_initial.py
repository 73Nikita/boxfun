# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BackCall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(max_length=255, verbose_name='\u041d\u043e\u043c\u0435\u0440')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0439 \u0437\u0432\u043e\u043d\u043e\u043a',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0435 \u0437\u0432\u043e\u043d\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('contacts', models.CharField(max_length=1024, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0439 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0435 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('contacts', models.CharField(max_length=1024, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435')),
                ('payment_status', models.PositiveSmallIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043e\u043f\u043b\u0430\u0442\u044b', choices=[(0, '\u0424\u043e\u0440\u043c\u0430 \u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u0430'), (1, '\u041d\u0430\u0436\u0430\u0442\u0430 \u043a\u043d\u043e\u043f\u043a\u0430 \u043e\u043f\u043b\u0430\u0442\u0438\u0442\u044c'), (2, '\u041e\u0442\u043a\u0430\u0437 \u043e\u0442 \u043e\u043f\u043b\u0430\u0442\u044b'), (3, '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c \u043e\u043f\u043b\u0430\u0442\u0438\u043b \u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0443')])),
                ('size', models.PositiveSmallIntegerField(default=0, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440', choices=[(0, b'L'), (1, b'XL'), (2, b'XXL')])),
                ('sex', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043b', choices=[(True, '\u0416\u0435\u043d\u0449\u0438\u043d\u0430'), (False, '\u041c\u0443\u0436\u0447\u0438\u043d\u0430')])),
                ('costume', models.BooleanField(default=False, verbose_name='\u0422\u0438\u043f \u043e\u0434\u0435\u0436\u0434\u044b', choices=[(True, '\u0422\u043e\u043b\u0441\u0442\u043e\u0432\u043a\u0430'), (False, '\u0424\u0443\u0442\u0431\u043e\u043b\u043a\u0430')])),
                ('order_type', models.SmallIntegerField(default=0, verbose_name='\u0422\u0438\u043f \u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0438', choices=[(0, '3 \u043c\u0435\u0441\u044f\u0446\u0430'), (1, '6 \u043c\u0435\u0441\u044f\u0446\u0435\u0432'), (2, '12 \u043c\u0435\u0441\u044f\u0446\u0435\u0432')])),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('contacts', models.CharField(max_length=1024, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435')),
                ('text', models.TextField(verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b',
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('contacts', models.CharField(max_length=1024, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435')),
                ('text', models.TextField(verbose_name='\u041e\u0442\u0437\u044b\u0432')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_one', models.FloatField(default=4980, help_text='\u0440\u0443\u0431', verbose_name='3 \u043c\u0435\u0441\u044f\u0446\u0430')),
                ('order_two', models.FloatField(default=17770, help_text='\u0440\u0443\u0431', verbose_name='6 \u043c\u0435\u0441\u044f\u0446\u0435\u0432')),
                ('order_three', models.FloatField(default=35550, help_text='\u0440\u0443\u0431', verbose_name='12 \u043c\u0435\u0441\u044f\u0446\u0435\u0432')),
            ],
            options={
                'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0430 \u0441\u0430\u0439\u0442\u0430',
                'verbose_name_plural': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0422\u0435\u043c\u0430\u0442\u0438\u043a\u0430 \u0434\u0438\u0437\u0430\u0439\u043d\u0430',
                'verbose_name_plural': '\u0422\u0435\u043c\u0430\u0442\u0438\u043a\u0438 \u0434\u0438\u0437\u0430\u0439\u043d\u0430',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='theme',
            field=models.ManyToManyField(to='core.Theme', verbose_name='\u0422\u0435\u043c\u0430\u0442\u0438\u043a\u0430 \u0434\u0438\u0437\u0430\u0439\u043d\u0430', blank=True),
        ),
    ]
