# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20151121_1358'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='old_price',
            field=models.FloatField(default=0, verbose_name='\u0421\u0443\u043c\u043c\u0430 \u043a \u043e\u043f\u043b\u0430\u0442\u0435', blank=True),
        ),
    ]
