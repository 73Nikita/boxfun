# coding: utf8
from django.views.generic import FormView, TemplateView
from django.shortcuts import redirect
from django.http import HttpResponse
from django.utils.http import urlunquote
from django.db.models import ObjectDoesNotExist
from collections import defaultdict
from hashlib import md5
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
import binascii
from forms import BackCallForm, QuestionForm, OrderForm, ReviewForm, CertificateForm
from models import Order, Settings, Result

SECRET_KEY = '71617744496536435b41745c5a5d49595d745037425e74424d475c'

FORM = {
    "WMI_MERCHANT_ID": "138194063012",
    "WMI_PAYMENT_AMOUNT": "",
    "WMI_CURRENCY_ID": "643",
    "WMI_SUCCESS_URL": "http://box-fun.ru",
    "WMI_FAIL_URL": "http://box-fun.ru",
    "WMI_PAYMENT_NO": "",
    "csrfmiddlewaretoken": "",
}


def get_signature(params, secret_key):
    icase_key = lambda s: unicode(s).lower()

    lists_by_keys = defaultdict(list)
    for key, value in params.iteritems():
        lists_by_keys[key].append(value)

    str_buff = ''
    for key in sorted(lists_by_keys, key=icase_key):
        for value in sorted(lists_by_keys[key], key=icase_key):
            str_buff += unicode(value).encode('1251')
    str_buff += secret_key
    md5_string = md5(str_buff).digest()
    return binascii.b2a_base64(md5_string)[:-1]


def get_format_price(price):
    price = str(int(price))
    format_price = ''
    for i in range(len(price) % 3 + 1):
        format_price = price[-3:] + ' ' + format_price
        price = price[:-3]
    return format_price[:-1]


class IndexView(FormView):
    template_name = 'index.html'
    form_type = None

    @method_decorator(csrf_exempt)
    @never_cache
    def dispatch(self, request, *args, **kwargs):
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        price = Settings.get_solo()
        context['price'] = {
            't_shirt_order_one': get_format_price(price.t_shirt_order_one),
            't_shirt_order_two': get_format_price(price.t_shirt_order_two),
            't_shirt_order_three': get_format_price(price.t_shirt_order_three),
            'sweatshirt_order_one': get_format_price(price.sweatshirt_order_one),
            'sweatshirt_order_two': get_format_price(price.sweatshirt_order_two),
            'sweatshirt_order_three': get_format_price(price.sweatshirt_order_three),
        }
        context['wmi_form'] = FORM
        return context

    def get_all_forms(self, form=None):
        all_forms = {
            'back_call_form': BackCallForm(),
            'question_form': QuestionForm(),
            'order_form': OrderForm(),
            'review_form': ReviewForm(),
            'certificate_form': CertificateForm(),
        }
        if form is not None:
            all_forms.update({self.form_type: form})
        return all_forms

    def get_form(self, form_class=None):
        form = None
        if self.form_type == 'back_call_form':
            form = BackCallForm(self.request.POST)
        elif self.form_type == 'question_form':
            form = QuestionForm(self.request.POST)
        elif self.form_type == 'order_form':
            form = OrderForm(self.request.POST)
        elif self.form_type == 'review_form':
            form = ReviewForm(self.request.POST)
        elif self.form_type == 'certificate_form':
            form = CertificateForm(self.request.POST)
        return form

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(**self.get_all_forms()))

    def post(self, request, *args, **kwargs):
        if 'ajax' in request.POST.keys():
            order = None
            try:
                order = Order.objects.get(id=request.POST.get('id'))
                order_form = OrderForm(request.POST, instance=order)
                if order_form.is_valid():
                    order = order_form.save()
            except:
                order_form = OrderForm(request.POST)
                if order_form.is_valid():
                    order = order_form.save()

            if order is not None:
                price = Settings.get_solo()
                if order.order_type == 0 and order.costume is False:
                    order.old_price = price.t_shirt_order_one
                elif order.order_type == 1 and order.costume is False:
                    order.old_price = price.t_shirt_order_two
                elif order.order_type == 2 and order.costume is False:
                    order.old_price = price.t_shirt_order_three
                elif order.order_type == 0 and order.costume is True:
                    order.old_price = price.sweatshirt_order_one
                elif order.order_type == 1 and order.costume is True:
                    order.old_price = price.sweatshirt_order_two
                elif order.order_type == 2 and order.costume is True:
                    order.old_price = price.sweatshirt_order_three
                order.save()
                form = FORM
                form['WMI_PAYMENT_NO'] = str(order.id)
                form['WMI_PAYMENT_AMOUNT'] = '%.2f' % order.old_price
                form['csrfmiddlewaretoken'] = request.POST.get('csrfmiddlewaretoken')
                return HttpResponse(
                    '%d:%.2f:%s' % (order.id, order.old_price, get_signature(form, SECRET_KEY)),
                    content_type='text/plain'
                )
            else:
                return HttpResponse(str(order), content_type='text/plain')

        self.form_type = request.POST.get('form_type')
        form = self.get_form(request.POST.get('form_type'))
        if form is not None:
            if form.is_valid():
                self.form_valid(form)
            else:
                return self.form_invalid(form)
        return redirect('core:index')

    def form_valid(self, form):
        form.save()
        return redirect('core:index')

    def form_invalid(self, form):
        all_forms = self.get_all_forms(form)
        return self.render_to_response(self.get_context_data(**all_forms))


class ResultView(FormView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ResultView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        Result.objects.create(data=str(request.GET))
        return redirect('core:index')

    def post(self, request, *args, **kwargs):
        Result.objects.create(data=str(request.POST))
        try:
            order = Order.objects.get(id=request.POST.get('WMI_PAYMENT_NO'))
            form = {}
            for field in request.POST.keys():
                form[field] = urlunquote(request.POST[field])
            del form['WMI_SIGNATURE']

            if request.POST.get('WMI_SIGNATURE') == urlunquote(get_signature(form, SECRET_KEY)):
                order.payment_status = 3
            else:
                order.payment_status = 2
            order.save()
        except ObjectDoesNotExist:
            pass
        return HttpResponse('WMI_RESULT=OK')


class VerificationView(TemplateView):
    def get(self, request, *args, **kwargs):
        return HttpResponse('', content_type='plain/text')
