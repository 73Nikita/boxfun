# -*- coding: utf-8 -*-

import os, sys, site
sys.path.insert(0, '/django/django-apps/boxfun/')
#sys.path.insert(0, os.path.dirname(__file__))
site.addsitedir('/django/virtualenv/boxfun/lib/python2.7/site-packages/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.env.production'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()